<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Socialite;
use Auth;
use Exception;
use App\User;

class LoginController extends Controller
{
		/*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /*** Where to redirect users after login. 
	** @var string 
	*/
    protected $redirectTo = '/home';
    /*** Create a new controller instance. 
	* * @return void 
	*/
    public function __construct() 
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToGoogle() 
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback() 
    {
        $user = Socialite::driver('google')->stateless()->user();
        try {
            
            dd($user);
            $finduser = User::where('google_id', $user->id)->first();
            if ($finduser) 
            {
                Auth::login($finduser);
                return redirect('/home');
            }
            else 
            {
                $newUser = User::create(['name' => $user->name, 'email' => $user->email, 'google_id' => $user->id]);
                Auth::login($newUser);
                return redirect()->back();
            }
        }
        catch(Exception $e) 
        {
            dd($e);
            //return redirect('auth/google');
        }
    }

    public function Facebooklogin() 
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback() 
    {
        $user = Socialite::driver('facebook')->user();

         dd($user);
    }
}
