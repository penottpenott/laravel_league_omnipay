<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Omnipay\Omnipay;

class OmnipayController extends Controller
{
	public function form_pagar_stripe() 
    {
        return view('form_payment_stripe');
    }

    public function pagar_stripe(Request $request)
    {  
    	//dd($request->all());

    	$fecha = explode("-", $request->expire_date);

    	$month_expire = $fecha[1];
    	$year_expire = $fecha[0];

	    $cardInput = [
			'number'      => $request->card,
			'firstName'   => $request->fullname,
			'expiryMonth' => $month_expire,
			'expiryYear'  => $year_expire,
			'cvv'         => $request->csv,
		];

		//dd($cardInput);

		$gateway = Omnipay::create('Stripe');

		$gateway->setApiKey('sk_test_0Vw2anA5AN0Ha62pwOSj46SL00ksIDRmOi');

		//$formData = array('number' => '4242424242424242', 'expiryMonth' => '6', 'expiryYear' => '2030', 'cvv' => '123');
		$response = $gateway->purchase(array('amount' => $request->amount, 'currency' => $request->currency,'token' => $request->card_tipe, 'card' => $cardInput))->send();


		if ($response->isSuccessful())
		{
	       echo "Purchase transaction was successful!\n";
	       $sale_id = $response->getTransactionReference();
	       echo "Transaction reference = " . $sale_id . "\n";
	
	       $balance_transaction_id = $response->getBalanceTransactionReference();
	       echo "Balance Transaction reference = " . $balance_transaction_id . "\n";
	    }

		//dd($response);
	}

	public function form_pagar_paypal() 
    {
        return view('form_payment_paypal');
    }

	public function pagar_paypal(Request $request)
    {  
   		$gateway = Omnipay::create('PayPal_Rest');
 
	    // Initialise the gateway
	    $gateway->initialize(array(
	        'clientId' => 'AXbt0yDfr_jaYfjWJHTxrLm-rPL7Y4jkG8EXwovHaAYiRZ5uHslQD1cF1Xc_WRSklgII0jAHO4XCONdh',
	        'secret'   => 'EH0X1sXfuLkma9YbR-di2_JNnt4-hSwHMzXDkyMnu1Ak09y93zLGRCR2HcmgasDIc1Y8pcGiUGCY-9Np',
	        'token'    => 'access_token$sandbox$6s5m6gnhn6y7yybc$1fe9263b46fb1a6a849a87cc37594750',
	        'testMode' => true, // Or false when you are ready for live transactions
	    ));

	    $fecha = explode("-", $request->expire_date);

    	$month_expire = $fecha[1];
    	$year_expire = $fecha[0];

	    

		$card = array(
	                'firstName' => $request->firstname,
	                'lastName' => $request->lastname,
	                'number' => $request->card,
	                'expiryMonth'           => $month_expire,
	                'expiryYear'            => $year_expire,
	                'cvv'                   => $request->csv,
	                'billingAddress1'       => '1 Scrubby Creek Road',
	                'billingCountry'        => 'AU',
	                'billingCity'           => 'Scrubby Creek',
	                'billingPostcode'       => '4999',
	                'billingState'          => 'QLD',
	   	);
		//	Authorize
		if(($request->transaction=='authorize')or($request->transaction=='capture'))
		{		

			$transaction_autorize = $gateway->authorize(array(
			        'amount'        => $request->amount,
			        'currency'      => 'USD',
			        'description'   => 'This is a test authorize transaction.',
			        'card'          => $card,
			));

		   	$response_aut = $transaction_autorize->send();

		   	if ($response_aut->isSuccessful()) {
		        // Find the authorization ID
		        $auth_id = $response_aut->getTransactionReference();
		        if($request->transaction=='authorize')
			    {
			    	return redirect()->route('pagado',['amount' => $request->amount,'operation' => 'Authorize']);
			    }
		    }   
		}

		//Capture (Si previamente se autorizo)
		if($request->transaction=='capture')
		{
		   	$transaction_capture = $gateway->capture(array(
		        'amount'        => $request->amount,
		        'currency'      => 'USD',
		    ));
		    $transaction_capture->setTransactionReference($auth_id);
		    $response_cap = $transaction_capture->send();

		    if ($response_aut->isSuccessful()) {
		        return redirect()->route('pagado',['amount' => $request->amount,'operation' => 'Authorize and Capture']);
		    }
		}

		// Directly Purchase
		if($request->transaction=='purchase')
		{
			
		    try {

			    $transaction_purchase = $gateway->purchase(array(
			        'amount'        => $request->amount,
			        'currency'      => 'USD',
			        'description'   => 'This is a test purchase transaction.',
			        'card'          => $card,
			    ));

			    $response_purchase = $transaction_purchase->send();
		        
		        if ($response_purchase->isSuccessful()) 
		        {
		          	return redirect()->route('pagado',['amount' => $request->amount,'operation' => 'Purchase']);
		        }
		    } 
		    catch (\Exception $e)
		    {
		        echo "Exception caught while attempting authorize.\n";
		        echo "Exception type == " . get_class($e) . "\n";
		        echo "Message == " . $e->getMessage() . "\n";
		    }
		}
		

		//dd($transaction);

		/* paypal express

		$gateway = Omnipay::create('PayPal_Express');
		$gateway->setUsername('sb-ftt5d3777093_api1.business.example.com');
		$gateway->setPassword('K3VLXKZHW8YY7P25');
		$gateway->setSignature('A-2571xtKc7zfa3gSrNtvhvIgpLZAl-ZDvNr0ZSqpXV1wWHlqZ3PJyBs');
		$gateway->setTestMode(true);

		$response = $gateway->purchase(array(
			'amount' => '15.00', 
			'currency' => 'USD', 
			'returnUrl' => 'http://localhost/laravel/public/pagar_paypal',
            'cancelUrl' => 'http://localhost/laravel/public/pagar_paypal',
		))->send();*/


		//dd($response);
	}

	public function pagado($amount,$operation) 
    {
        return view('pagado')->with(['amount' => $amount,'operation' => $operation]);
    }

    public function purchases()
    {  
    	$gateway = Omnipay::create('PayPal_Rest');
 
	    // Initialise the gateway
	    $gateway->initialize(array(
	        'clientId' => 'AXbt0yDfr_jaYfjWJHTxrLm-rPL7Y4jkG8EXwovHaAYiRZ5uHslQD1cF1Xc_WRSklgII0jAHO4XCONdh',
	        'secret'   => 'EH0X1sXfuLkma9YbR-di2_JNnt4-hSwHMzXDkyMnu1Ak09y93zLGRCR2HcmgasDIc1Y8pcGiUGCY-9Np',
	        'token'    => 'access_token$sandbox$6s5m6gnhn6y7yybc$1fe9263b46fb1a6a849a87cc37594750',
	        'testMode' => true, // Or false when you are ready for live transactions
	    ));

	     // Make some DateTimes for start and end times
	     $start_time = new \DateTime('2020-11-15');
	     $end_time = new \DateTime('now');
	  
	     // List the transaction so that details can be found for refund, etc.
	     $transaction = $gateway->listPurchase(array(
	         'startTime' => $start_time,
	         'endTime'   => $end_time
	     ));
	     $response = $transaction->send();
	     $data = $response->getData();

	     //dd($data);

	     $collection = collect([]);

	     foreach ($data['payments'] as $pay)
	     {
	     	$state =  'N/D';
	     	$comision = 'N/D';

	     	

	     	$id = $pay['id'];

	     	$state =  $pay['state'];

	     	$fecha =  $pay['create_time'];

	     	foreach ($pay['transactions'] as $trans) 
	     	{
	     		$monto = $trans['amount']['total'];
	     		$moneda = $trans['amount']['currency'];
	     		$description =  $trans['description'];
	     		$pagador =  $trans['soft_descriptor'];

	     		//dd($trans['related_resources']);

	     		foreach ($trans['related_resources'] as $r) 
	     		{
	     			if(isset($r['sale']['transaction_fee']['value']))
	     			{
	     				$comision = $r['sale']['transaction_fee']['value'];
	     			}
	     		}
	     	}

	     	$collection->push(
			    ['id' => $id, 'monto' => $monto, 'moneda' => $moneda, 'description' => $description, 'pagador' => $pagador, 'state' =>  $state,'comision' => $comision,'fecha' => $fecha ]
			);
	     	
	     }

	     return view('purchases')->with(['collection' => $collection]);
	     //echo "Gateway listPurchase response data == " . print_r($data, true) . "\n";
	}

}
