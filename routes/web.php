<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/pagina_principal', [                   
			'uses' => 'WelcomeController@welcome',  
			'as'   => 'welcome'        
]);

Route::get('google', 'LoginController@redirectToGoogle')->name('login_google');
Route::get('google/callback', 'LoginController@handleGoogleCallback'); 

Route::get('facebook', 'LoginController@Facebooklogin')->name('login_face');
Route::get('facebook/callback', 'LoginController@handleFacebookCallback'); 

Route::get('/form_pagar_stripe', [                    
	'uses' => 'OmnipayController@form_pagar_stripe',  
	'as'   => 'form_pagar_stripe'        
]);

Route::post('/pagar_stripe', [                    
	'uses' => 'OmnipayController@pagar_stripe',  
	'as'   => 'pagar_stripe'        
]);

Route::get('/form_pagar_paypal', [                    
	'uses' => 'OmnipayController@form_pagar_paypal',  
	'as'   => 'form_pagar_paypal'        
]);

Route::post('/pagar_paypal', [                    
	'uses' => 'OmnipayController@pagar_paypal',  
	'as'   => 'pagar_paypal'        
]);

Route::get('/pagar_payment_express', [                    
	'uses' => 'OmnipayController@pagar_payment_express',  
	'as'   => 'pagar_payment_express'        
]);

Route::get('/pagado/{amount}/{operation}', [                    
	'uses' => 'OmnipayController@pagado',  
	'as'   => 'pagado'        
]);

Route::get('/list_purchases', [                    
	'uses' => 'OmnipayController@purchases',  
	'as'   => 'purchases'        
]);