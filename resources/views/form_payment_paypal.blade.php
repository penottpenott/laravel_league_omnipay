<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Basic Forms - Robust Free Bootstrap Admin Template</title>
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/images/ico/apple-icon-60.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/images/ico/apple-icon-76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/images/ico/apple-icon-120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/images/ico/apple-icon-152.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/ico/favicon.ico') }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/images/ico/favicon-32.png') }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.css') }}">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/icomoon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/pace.css') }}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/colors.css') }}">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/core/menu/menu-types/vertical-overlay-menu.css') }}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('pro/css/style.css') }}">
    <!-- END Custom CSS-->
  </script>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
            <li class="nav-item"><a href="index.html" class="navbar-brand nav-link"><img alt="branding logo" src="{{ asset('assets/images/logo/robust-logo-light.png') }}" data-expand="{{ asset('assets/images/logo/robust-logo-light.png') }}" data-collapse="{{ asset('assets/images/logo/robust-logo-small.png') }}" class="brand-logo"></a></li>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
          </ul>
        </div>
        <div class="navbar-container content container-fluid">
          <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
            <ul class="nav navbar-nav float-xs-right">
              <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online"><img src="{{ asset('assets/images/portrait/small/avatar-s-1.png') }}" alt="avatar"><i></i></span><span class="user-name">John Doe</span></a>
                <div class="dropdown-menu dropdown-menu-right"><a href="#" class="dropdown-item"><i class="icon-head"></i> Edit Profile</a><a href="#" class="dropdown-item"><i class="icon-mail6"></i> My Inbox</a><a href="#" class="dropdown-item"><i class="icon-clipboard2"></i> Task</a><a href="#" class="dropdown-item"><i class="icon-calendar5"></i> Calender</a>
                  <div class="dropdown-divider"></div><a href="#" class="dropdown-item"><i class="icon-power3"></i> Logout</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- main menu-->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
      <!-- main menu header-->
      <div class="main-menu-header">
        <input type="text" placeholder="Search" class="menu-search form-control round"/>
      </div>
      <!-- / main menu header-->
      <!-- main menu content-->
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class=" nav-item"><a href="index.html"><i class="icon-money"></i><span data-i18n="nav.dash.main" class="menu-title">Formas de Pago</span></a>
            <ul class="menu-content">
              <li><a href="{{ route('form_pagar_stripe')}}" data-i18n="nav.dash.main" class="menu-item">Stripe</a>
              </li>
              <li class="active"><a href="{{ route('form_pagar_paypal')}}" data-i18n="nav.dash.main" class="menu-item">Paypal</a>
              </li>
            </ul>
          </li>
           <li class=" nav-item"><a href="index.html"><i class="icon-money"></i><span data-i18n="nav.dash.main" class="menu-title">Listas de Pagos</span></a>
            <ul class="menu-content">
              <li><a href="{{ route('purchases')}}" data-i18n="nav.dash.main" class="menu-item">Paypal</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /main menu content-->
      <!-- main menu footer-->
      <!-- include includes/menu-footer-->
      <!-- main menu footer-->
    </div>
    <!-- / main menu-->

    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Basic Forms</h2>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Form Layouts</a>
                </li>
                <li class="breadcrumb-item active"><a href="#">Basic Forms</a>
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Basic form layout section start -->
<section id="basic-form-layouts">


	<div class="row match-height">
		<div class="col-md-6 offset-md-3">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title" id="basic-layout-square-controls">Donation</h4>
					<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
							<li><a data-action="reload"><i class="icon-reload"></i></a></li>
							<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
							<li><a data-action="close"><i class="icon-cross2"></i></a></li>
						</ul>
					</div>
				</div>
          <form class="form-horizontal form-simple" name="Form" id="Form" method="POST" action="{{ route('pagar_paypal') }}" novalidate>
                        {{ csrf_field() }}
    				<div class="card-body collapse in">
    					<div class="card-block">

    						<div class="card-text">
    							<p>Ingrese sus Datos</p>
    						</div>

    						<form class="form">
    							<div class="form-body">
                    <div class="form-group">
                      <label for="issueinput6">Transaccion</label>
                      <select id="transaction" name="transaction" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Status">
                        <option value="">Seleccione</option>
                        <option value="authorize">Authorize</option>
                        <option value="capture">Authorize and Capture</option>
                        <option value="purchase">Purchase</option>
                      </select>
                    </div>
    								<div class="form-group">
    									<label for="donationinput1">Nombre</label>
    									<input type="text" id="firstname" class="form-control square" placeholder="name" name="firstname">
    								</div>
                    <div class="form-group">
                      <label for="donationinput1">Apellido</label>
                      <input type="text" id="lastname" class="form-control square" placeholder="name" name="lastname">
                    </div>
    								<div class="form-group">
    									<label for="donationinput2">Email</label>
    									<input type="email" id="email" class="form-control square" placeholder="email" name="email">
    								</div>

    								<div class="form-group">
    									<label for="donationinput3">Numero de Contacto</label>
    									<input type="tel" id="contact" class="form-control square" name="contact">
    								</div>

                    <div class="form-group">
                      <label for="issueinput6">Tipo de Tarjeta</label>
                      <select id="card_tipe" name="card_tipe" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Status">
                        <option value="">Seleccione</option>
                        <option value="visa">Visa</option>
                        <option value="mastercard">Mastercard</option>
                        <option value="amex">American Express</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="donationinput4">Numero de la Tarjeta</label>
                      <div id="number_card">
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="complaintinput3">Fecha de Expiracion</label>
                      <div id="fecha_exp">
                        
                      </div>
                      
                    </div>

                    <div class="form-group">
                      <label for="donationinput3">CSV number</label>
                      <div id="csv_numb">
                        
                      </div>
                    </div>

    								<div class="form-group">
    									<label>Monto</label>
    									<div class="input-group">
    										<span class="input-group-addon">$</span>
    										<input type="number" class="form-control square" placeholder="amount" aria-label="Amount (to the nearest dollar)" name="amount">
    										<span class="input-group-addon">.00</span>
    									</div>
    								</div>

    							</div>

    							<div class="form-actions right">
    								<button type="button" class="btn btn-warning mr-1">
    									<i class="icon-cross2"></i> Cancel
    								</button>
    								<button type="submit" class="btn btn-primary">
    									<i class="icon-check2"></i> Save
    								</button>
    							</div>
    						</form>

    					</div>
    				</div>
          </form>
			</div>
		</div>
	</div>




</section>
<!-- // Basic form layout section end -->
        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="{{ asset('assets/js/core/libraries/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/js/ui/tether.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/js/ui/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/js/ui/unison.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/js/ui/blockUI.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/js/ui/jquery.matchHeight-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/js/ui/screenfull.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/js/extensions/pace.min.js') }}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="{{ asset('assets/js/core/app-menu.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/core/app.js') }}" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->

    <script type="text/javascript">

      $("#card_tipe").change(function(event){
        var card_tipe = $( "#card_tipe" ).val();
        if(card_tipe=="visa")
        {
          $("#number_card").empty();
          $("#fecha_exp").empty();
          $("#csv_numb").empty();
          $("#number_card").append("<input type='number' id='card' class='form-control square' name='card' value='4032035158840504' readonly>");
          $("#fecha_exp").append("<input type='date' id='expire_date' class='form-control round' name='expire_date' value='2021-11-01' readonly>");
          $("#csv_numb").append("<input type='number' value='866' id='csv' class='form-control square' name='csv' readonly>");
          
        }
        if(card_tipe=="mastercard")
        {
          $("#number_card").empty();
          $("#fecha_exp").empty();
          $("#csv_numb").empty();
          $("#number_card").append("<input type='number' id='card' class='form-control square' name='card' value='5110920337399021' readonly>");
          $("#fecha_exp").append("<input type='date' id='expire_date' class='form-control round' name='expire_date' value='2024-09-01' readonly>");
          $("#csv_numb").append("<input type='number' value='621' id='csv' class='form-control square' name='csv' readonly>");
        }
        if(card_tipe=="amex")
        {
          $("#number_card").empty();
          $("#fecha_exp").empty();
          $("#csv_numb").empty();
          $("#number_card").append("<input type='number' id='card' class='form-control square' name='card' value='377766212505755' readonly>");
          $("#fecha_exp").append("<input type='date' id='expire_date' class='form-control round' name='expire_date' value='2023-05-01' readonly>");
          $("#csv_numb").append("<input type='number' value='5044' id='csv' class='form-control square' name='csv' readonly>");
        }
        
        if(card_tipe=="")
        {
          $("#number_card").empty();
          $("#fecha_exp").empty();
          $("#csv_numb").empty();
        }
      });
    </script>
  </body>
</html>
